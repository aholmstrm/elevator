import * as Koa from "koa";
import * as bodyparser from "koa-bodyparser";
import * as cors from "kcors";
import * as Router from "koa-router";
import { elevatorState, addRequest, createShafts, elevatorEngine, elevatorSchedule } from "./utils/elevator";

const app = new Koa();
const router = new Router();

router.get("/sample", (context) => {
    Object.assign(context.response, {
        body: {message: "Hello world"},
        status: 200,
    });
});

router.get("/elevators", (context) => {
  Object.assign(context.response, {
      body: elevatorState,
      status: 200,
  });
});

router.post("/elevators", (context) => {
  // TODO clean params
  addRequest(context.request.body)
  Object.assign(context.response, {
      status: 201,
  });
});

app.use(bodyparser({
    enableTypes: ["json"]
}))

app.use(cors());

app.use(router.routes());

app.listen(3000);

const startElevator = () => {
  createShafts();
  elevatorEngine();
  elevatorSchedule();
};

startElevator();
