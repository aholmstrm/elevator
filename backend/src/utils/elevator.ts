const directions = {
  up: "up",
  down: "down"
};

const delayMilliseconds = 2 * 1000;
const elevatorState = [];
let elevatorRequests = [];

const getShafts = ((amount, maxFloor) => {
  return [...Array(amount)].map((_, index) => ({ id: index + 1, maxFloor, currentFloor: 1, task: null  }));
});

const createShafts = ((amount=5, maxFloor=20) => {
  elevatorState.push(...getShafts(amount, maxFloor));
});

const addRequest = ((request) => {
  console.info(`Add request: ${JSON.stringify(request)}`);
  elevatorRequests.push(request);
});

const elevatorSchedule = () => {
  elevatorRequests.forEach((request) => {
    console.log(request);
    if (rulePassBy(request)) return;
    if (ruleClosestAvailable(request)) return;
  });

  setTimeout(elevatorSchedule, delayMilliseconds);
}

const creatTask = (element, direction, selectedElevator) => {
  selectedElevator.task = { direction: direction, floors: [element.floor] };
  // remove from queue!
  elevatorRequests = elevatorRequests.filter((request) => request !== element);
}

const incrementTask = (element, selectedElevator) => {
  selectedElevator.task.floors.push(element.floor);
  // remove from queue!
  elevatorRequests = elevatorRequests.filter((request) => request !== element);
}

const rulePassBy = (request) => {
  const selectedElevator = elevatorState.reduce((result, current) => {
    if (current.task && current.task.direction === request.direction) {
      if (
        request.direction === directions.up && // up
        current.currentFloor < request.floor && // not already passed by?
        request.floor < Math.max.apply(null, current.task.floors) && // will pass by?
        (result === null || result.currentFloor < current.currentFloor) // its current closest?
      ) {
        return current;
      }

      if (
        request.direction === directions.down && // down
        current.currentFloor > request.floor && // not already passed by?
        request.floor > Math.min.apply(null, current.task.floors) && // will pass by?
        (result === null || result.currentFloor > current.currentFloor) // its current closest?
      ) {
        return current;
      }
    }
    return result;
  }, null);

  if (selectedElevator !== null) {
    console.info("Apply rule: Pass by");
    incrementTask(request, selectedElevator);
    return true;
  }
  return false;
}

const ruleClosestAvailable = (request) => {
  const selectedElevator = elevatorState.reduce((result, current) => {
    console.log(request);
    if (result === null && current.task === null) return current; // select current elevator
    if (
      result !== null && // other elevator available
      Math.abs(current.currentFloor - request.floor) < Math.abs(result.currentFloor - request.floor) && // select closest elevator
      current.task === null  // no task (available)
    ) {
      return current;
    }
    return result;
  }, null);

  const direction = request.floor > selectedElevator.currentFloor ? directions.up : directions.down;
  if (selectedElevator !== null) {
    console.info("Apply rule: Closest available");
    creatTask(request, direction, selectedElevator);
    return true;
  }

  return false;
}

const elevatorEngine = () => {
  elevatorState.forEach((elevator) => {
    console.log(elevator);
    updateState(elevator);
  });

  console.info("heartbeat");
  setTimeout(elevatorEngine, delayMilliseconds);
}

const goUp = (elevator) => {
  console.log(directions.up);
  // max value
  const max = Math.max.apply(null, elevator.task.floors);
  console.log(max);
  // current floor (delete task)
  if (elevator.currentFloor === max) {
    elevator.task = null;
    return;
  }
  // move one level up
  elevator.currentFloor += 1;
}

const goDown = (elevator) => {
  console.log(directions.down);
  // min value
  const min = Math.min.apply(null, elevator.task.floors);
  console.log(min);
  // current floor (delete task)
  if (elevator.currentFloor === min) {
    elevator.task = null;
    return;
  }
  // move one level down
  elevator.currentFloor -= 1;
}

const updateState = (elevator) => {
  if (elevator.task && elevator.task.direction === directions.up) goUp(elevator);
  if (elevator.task && elevator.task.direction === directions.down) goDown(elevator);
}

export { elevatorState,
         addRequest,
         createShafts,
         elevatorEngine,
         elevatorSchedule
};
