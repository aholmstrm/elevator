import * as React from "react";
import Floor from "./Floor";

import * as css from "./Shaft.module.scss";

interface IProperties {
  floors: number;
  currentFloor: number;
  available: boolean;
}

const Shaft = (props: IProperties) => {
  const { floors, currentFloor, available } = props;
  return (
    <div className={css.column}>
    {[...Array(floors)].map((_, index) => (
      <Floor key={index} currentFloor={currentFloor === floors - index} available={available} />
    ))}
    </div>
  );
};

export default Shaft;
