import * as React from "react";
import Building from "./Building";

class ImplementationPage extends React.Component {
    public render() {
        return (
            <>
              <h2>Lägg implementationen här</h2>
              <Building />
            </>
        );
    }
}

export default ImplementationPage;
