import * as React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDoorOpen, faDoorClosed } from '@fortawesome/free-solid-svg-icons'

import * as css from "./Floor.module.scss";

interface IProperties {
  currentFloor: boolean;
  available: boolean;
}

const Floor = (props: IProperties) => {
  const { currentFloor, available } = props;

  const doorStyle = (currentFloor: Boolean, available: Boolean) => {
    if (currentFloor && available) return { door: faDoorOpen, style: css.available };
    if (currentFloor && !available) return { door: faDoorClosed, style: css.unavailable };
    return { door: faDoorClosed, style: css.empty };
  };

  const { door, style } = doorStyle(currentFloor, available);

  return (
    <div className={`${css.floor} ${style}`}>
      <FontAwesomeIcon icon={door} size="lg" />
    </div>
  );
};

export default Floor;
