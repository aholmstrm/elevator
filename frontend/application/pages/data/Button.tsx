import * as React from "react";
import { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowUp, faArrowDown } from '@fortawesome/free-solid-svg-icons'

import * as css from "./Button.module.scss";

interface IProperties {
  direction: string;
  floor: number;
  arrived: boolean;
}

const Button = (props: IProperties) => {
  const directions = {
    up: "up",
    down: "down"
  };

  const [requested, setRequested] = useState(false);
  const { direction, floor, arrived } = props;

  const httpCall = async (url : string, data : any = {}) => {
    const request = data;
    request.headers = {
      "Content-Type": "application/json",
    };
    request.body = JSON.stringify(data.body);
    const response = await fetch(url, request);
    return response;
  };

  const handleClick = async () => {
    const request = { method: "POST", body: { direction, floor} };
    await httpCall("http://localhost:3000/elevators", request);
    setRequested(true);
  };

  const elevatorArrived = () => {
    if (arrived && requested) setRequested(false);
  };

  elevatorArrived();

  return (
    <button className={[css.btnElevator, requested ? css.requested : ""].join(" ")}
      href="#"
      onClick={handleClick}>
        <FontAwesomeIcon
          icon={ direction === directions.up ? faArrowUp : faArrowDown }
      />
    </button>
  );
};

export default Button;
