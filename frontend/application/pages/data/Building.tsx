import * as React from "react";
import { useState, useEffect } from "react";
import Shaft from "./Shaft";
import Button from "./Button";

import * as css from "./Building.module.scss";

const Building = () => {
  const directions = {
    up: "up",
    down: "down"
  };

  const [initialized, setInitialized] = useState(false);
  const [elevatorState, setElevatorState] = useState([]);
  const [floors, setFloors] = useState(0);
  const delayMilliseconds = 2 * 1000;

  const updateGui = async () => {
    // dirty polling
    const response = await fetch("http://localhost:3000/elevators");
    const text = await response.text();
    const data = JSON.parse(text);

    setElevatorState(data);
    setFloors(data[0].maxFloor);

    console.log("heartbeat");
    setTimeout(updateGui, delayMilliseconds);
  }

  const elevatorArrived = (floor: number) => {
    return elevatorState.reduce((result, current) => {
      return result || current.currentFloor === floor;
    }, false);
  };

  useEffect(() => {
    if (!initialized) {
      updateGui();
      setInitialized(true);
    }
  }, [initialized]);

  return (
    <>
      <h1>Building</h1>
      <div className={css.column}>
        {[...Array(floors)].map((_, index) => (
          <div key={index} className={css.floor}>
            <span className={css.floorNumber}>{floors - index}.</span>
            <Button
              direction={directions.up}
              arrived={elevatorArrived(floors - index)}
              floor={floors - index}
            />
            <Button
              direction={directions.down}
              arrived={elevatorArrived(floors - index)}
              floor={floors - index}
            />
          </div>
        ))}
      </div>
      {elevatorState.map((element, index) => (
        <Shaft
          key={index}
          floors={element.maxFloor}
          available={element.task === null}
          currentFloor={element.currentFloor}
        />
      ))}
      <span className={css.clear}></span>
    </>
  );
};

export default Building;
